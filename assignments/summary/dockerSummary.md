# **DOCKER**
![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS6bhzHjTyl9IyOUN7a4oozAPcHSe6Ktrm_Ww&usqp=CAU)

## What is Docker?
Docker is a tool used to deploy and run the applications over different environments.It uses containers to do this.Containers are teh one which packages the application being developed,along with all the necessary tools and libraries which are necessary for the application.

## Docker v/s VM 
![](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/10/virtual-machine-vs-docker-example-what-is-docker-container-edureka-1.png)

In case of VM whole OS is sent along with the application which consumes more memory but in case of docker only necessary files are packaged into the container.

## Architecture
![client server architecture of docker](https://docs.docker.com/engine/images/architecture.svg)

The architecture is a client server architecture.where client can get the docker image from the docker server. Pull it from there and can run in any environment.

## Docker work flow
![](https://i.pinimg.com/originals/e8/12/7c/e8127c8baaa71be9a42ca73515b11a77.jpg)

One has to specify the application and all the necessary requirements into the docker file,which should be converted to docker image upon running which docker containers are formed,that is docker containers are the run time instances of the docker images.Hence one can store the docker images in cloud called as docker hub and containers can be easily made available to access by others.

## Docker container
![Docker container](https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png?itok=vle7kjDj)

A single Docker container can contain any number of applications and all the requirements of that application.

### Basic container management commands
![](https://raw.githubusercontent.com/sangam14/dockercheatsheets/master/dockercheatsheet1.png) 




