# **GIT**
## What is Git?
![](https://www.linuxjournal.com/sites/default/files/styles/360_250/public/nodeimage/story/git-icon.png?itok=w7zB9vuE)

**Git is a Distributed Version Controle Software, which will manage the changes made to the files and also coordinates different people's work on the same file.It is very useful in keeping track of different versions of the source code developed in application development.**

## Features of Git

![](https://image.slidesharecdn.com/whatisgit-devopstutorial-edureka-170525072621/95/what-is-git-what-is-github-git-tutorial-github-tutorial-devops-tutorial-edureka-21-638.jpg?cb=1495698093)
 

- **Distributed:It provides local repository**
- **compatible with any systems and other version controle systems too.**
- **Nonlinear because it stores the data in tree structure.**
- **It provides collaboration**

![collaboration and version storage](https://git-scm.com/book/en/v2/images/centralized.png)

- **stores the versions:It keeps track of all the versions ,what exactly is the difference between versions and at any time one can go to the previous versions to work with.So one has no need to worry about the changes made to versions and the name of versions at all.**
- **If the central server crashes,it always provides backup.**
- **It provides all the information to analyse the project.**
- **More over Git is an open source.**

## Git and Github:

**git is a version controle tool whereas github is a social network where the projects people work on will be shared,it is like a remote storage.**

## Repository

**It is a directory or a storage space where the projects and files have been stored.**

![](https://image.slidesharecdn.com/whatisgit-devopstutorial-edureka-170525072621/95/what-is-git-what-is-github-git-tutorial-github-tutorial-devops-tutorial-edureka-33-638.jpg?cb=1495698093)

**Local repository**:If the repository is a storage space in a local machine.
**Central repositoy** :If the repositoy is in the server or network platforms like github.

## Git operations and commands

![Git operations](https://qph.fs.quoracdn.net/main-qimg-d151c0543baa145e6252c1ec95199963.webp)

![Basic Git commands](https://image.slidesharecdn.com/a-cheat-sheet-for-git-170711120010/95/git-cheat-sheet-4-638.jpg?cb=1499774649)




